<?php

class Publication {

    public $id;
    public $title;
    public $date;
    public $short_content;
    public $content;
    public $preview;
    public $author_name;
    public $type;

    function __construct($row) {
        $this->id = $row['id'];
        $this->title = $row['title'];
        $this->data = $row['data'];
        $this->short_content = $row['short_content'];
        $this->content = $row['content'];
        $this->preview = $row['preview'];
        $this->author_name = $row['author_name'];
        $this->type = $row['type'];
    }

}

class NewsPublication extends Publication {

    public function printItem() {
        echo '<br>Новость: ' . $this->title;
        echo '<br>Дата: ' . $this->data;
        echo '<br>Автор: ' . $this->author_name;
        echo '<hr>';
    }

}

class ArticlePublication extends Publication {

    public function printItem() {
        echo '<br>Новость: ' . $this->title;
        echo '<br>Дата: ' . $this->author_name;
        echo '<hr>';
    }

}

class PhotoReportPublication extends Publication {

    public function printItem() {
        echo '<br>Новость: ' . $this->title;
        echo '<br>Контент: ' . $this->content;
        echo '<hr>';
    }

}
