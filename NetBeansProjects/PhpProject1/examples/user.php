<?php

class User 
{
    public $firstname;
    public $lastname;   
}

function getFullName ($user) 
{
    return $user->firstname.' '.$user->lastname;
}

$user1 = new User;
$user1->firstname = 'Artem';
$user1->lastname = 'Shtol';

echo getFullName($user1);

class SuperUser extends User 
{
    
}

$user2 = new SuperUser;
$user2->firstname = 'Andrew';
$user2->lastname = 'Brocken';

echo getFullName($user2);