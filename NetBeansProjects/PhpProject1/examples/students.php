<?php

class Student {
    
    public $name;
    public $results;
    
    function __construct($name, array $results) {
        echo '<br>Student '.$name.': ';
        foreach ($results as $subject => $item){
            echo '<br>'.$subject.': '.$item;
        }
        echo '<hr>';
    }  
}

     $student1 = new Student('Artem', array('Mathematic' => 3, 'Physik' => 5)); 
     $student2 = new Student('Alex', array('Mathematic' => 5, 'Physik' => 3)); 
     $student3 = new Student('Victor',1);
     
     #Eto proverka tipa dannbIx.