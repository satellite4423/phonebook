<?php
include_once ROOT.'/components/Db.php';
class News {

    public static function getNewsItemById($id) {

        $id = intval($id);

        if ($id) {
//          $host = 'example.com';
//          $dbname = 'Phonebook';
//          $user = 'root';
//          $password = 'password';
//          $db = new PDO("mysql:host=$host; dbname=$dbname", $user, $password);
            $db = Db::getConnection();

            $result = $db->query(' SELECT * FROM publication WHERE id=' . $id);

            // $result->setFetchMode(PDO::FETCH_NUM);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            $newsItem = $result->fetch();

            return $newsItem;
        }
    }

    public static function getNewsList() {
//        $host = 'example.com';
//        $dbname = 'Phonebook';
//        $user = 'root';
//        $password = 'password';
//        $db = new PDO("mysql:host=$host; dbname=$dbname", $user, $password);
        $db = Db::getConnection();
        $newsList = array();

        $result = $db->query(' SELECT id, title, date, short_content FROM publication ORDER BY date DESC LIMIT 10 ');

        $i = 0;

        while ($row = $result->fetch()) {
            $newsList[$i] ['id'] = $row['id'];
            $newsList[$i] ['title'] = $row['title'];
            $newsList[$i] ['date'] = $row['date'];
            $newsList[$i] ['short_content'] = $row['short_content'];
            $i++;
        }
        return $newsList;
    }

}
