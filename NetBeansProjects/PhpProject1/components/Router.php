<?php

use NewsController;

class Router {

    private $routes;    // Массив вызовов который находиться в отдельном файле.

    public function __construct() {             // Конструктор без параметров, который позволяет взять запросы (news) и хранить их в переменной.
        $routesPath = ROOT . '/config/routes.php';   // Указываем переменной путь к файлу маршрутов
        $this->routes = include ($routesPath);          // Включаем роуты(запросы) в переменную.
    }

    private function getURI() {              //функция которая берет адрес запроса (помещая в глоб переменную SERVER) 
        if (!empty($_SERVER['REQUEST_URI'])) {  //Условие если адрес запроса не пустой,       
            return trim($_SERVER['REQUEST_URI'], '/');  // то возвращаем запрос разделенный '/'
        }
    }

    public function run() {         // Метод управления, от фронт контроллера.
        $uri = $this->getURI();     // помещаем адрес запроса в переменую ури.

        foreach ($this->routes as $uriPattern => $path) {   // цикл. Маршруты помещаем в переменную ури паттерн(как шаблон для сравнения)
                                                                                                // переменная патч это элемент обработчик. (типа индекс массива и значения =>)
            if (preg_match("~$uriPattern~", $uri)) {          // ищет совпадения урипатерна в ури.
//               
//                 echo '<br>Где ищем (запрос который набрал пользователь): '.$uri;
//                echo '<br>Что ищем (совпадения из правил): '.$uriPattern;
//                echo '<br>Кто обрабатывает: '.$path;
                
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri); //функция находит совпадения и замещает патч
//              
//                  echo '<br>Нужно сформировать: '.$internalRoute.'<br>';
                
                $segments = explode('/', $internalRoute);   // Разбиваем строку с помощью разделителя.
                $controllerName = array_shift($segments) . 'Controller';  // 
                $controllerName = ucfirst($controllerName);
                $actionName = 'action' . ucfirst(array_shift($segments));
                
//                echo 'controller name: '.$controllerName.'<br>';
//                echo 'action name: '.$actionName;
                
                $parametrs = $segments;
                
//                echo '<pre>';
//                print_r($parametrs);
                
             
                $controllerFile = ROOT . '/controller/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once ($controllerFile);
                }
                
                
                $controllerObject = new $controllerName;
                $result = call_user_func_array(array($controllerObject, $actionName), $parametrs);
                
                
                if ($result != null) {
                    break;
                }
                
                
            }
        }
    }

}
