<?php
require_once dirname('/home/artem/NetBeansProjects/phonebook.com') . "/phonebook.com/bootstrap.php";
require_once dirname('/home/artem/NetBeansProjects/phonebook.com') . "/phonebook.com/src/Product.php";

$id = 1;
$newName = 'Namename';

$product = $entityManager->find('Product', $id);

if ($product === null) {
    echo "Product $id does not exists. \n";
    exit(1);
}
$product->setName($newName);
$entityManager->flush();
