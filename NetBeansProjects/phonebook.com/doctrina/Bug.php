<?php

use Doctrine\Common\Collections\ArrayCollection;

require_once dirname('/home/artem/NetBeansProjects/phonebook.com') . "/phonebook.com/src/Product.php";
require_once dirname('/home/artem/NetBeansProjects/phonebook.com') . "/phonebook.com/src/User.php";

class Bug
{
/**
 *
 * 
 */
    protected $id;
    protected $description;
    protected $created;
    protected $status;

    public function getId()
    {
        $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated(DateTime $created)
    {
        $this->created = $created;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($created)
    {
        $this->created = $created;
    }

//    protected $products;
//
//    public function __construct() {
//        $this->products = new ArrayCollection();
//    }

    protected $engineer;
    protected $reporter;

    public function setEngineer($engineer)
    {
        $engineer->assignedToBug($this);
        $this->engineer = $engineer;
    }

    public function getEnginner()
    {
        return $this->engineer;
    }

    public function setReporter($reporter)
    {
        $this->reporter = $reporter;
    }

    public function getReporter()
    {
        return $this->reporter;
    }

    protected $products = null;
    protected $product;

    public function assignToProduct($product)
    {
        $this->products[] = $product;
    }

    public function getProducts()
    {
        return $this->products;
    }

}
