<?php
$dql = "SELECT b,e,r FROM Bug b JOIN b.engineer e JOIN b.reporter r ORDER BY b.created DESC";

$query = $entityManager->createQuery($dql);
$query->setMaxResults(30);
$bugs = $query->getResult();
foreach ($bugs as $bug){
    echo $bug->getDescription()." - ".$bug->getCreated()->format('d.m.Y')."\n";
    echo " Reported by: ".$bug->getReporter()->getName()."\n";
    echo " Assignes by: ".$bug->getEngineer()->getName()."\n";

    foreach ($bug->getProducts() as $product){
        echo " PLatform:  ".$product->getName("asd")."\n";
    }
    echo "\n";
}

use Doctrine\ORM\Tools\SchemaValidator;
$validator = new SchemaValidator($entityManager);
$errors = $validator->validateMapping();    