<?php
require_once dirname('/home/artem/NetBeansProjects/phonebook.com') . "/phonebook.com/bootstrap.php";
require_once dirname('/home/artem/NetBeansProjects/phonebook.com') . "/phonebook.com/src/Product.php";
use Doctrine\ORM\EntityManager;

$newUsername = $argv[1];
$user = new User();
$user->setName($newUsername);

$entityManager->persist($user);
$entityManager->flush();
echo "Created User with ID " . $user->getId() . "\n";