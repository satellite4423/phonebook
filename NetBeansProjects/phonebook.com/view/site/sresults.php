<!DOCTYPE html>
<?php require_once ROOT . '/models/users.php'; ?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Результаты поиска</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">
    <link href="/css/price-range.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
    <link href="/css/page.css" rel="stylesheet">
</head>
<body>
<header><?php include_once ROOT . '/view/site/header.php'; ?></header>
<table class="table-bordered table-striped table">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Age</th>
        <th>Country</th>
        <th>City</th>
        <th>Number</th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($get as $resultsearch): ?>
        <tr>
            <td><?php echo $resultsearch['user_id']; ?></td>
            <td><?php echo $resultsearch['firstname']; ?> </td>
            <td><?php echo $resultsearch['lastname']; ?></td>
            <td><?php echo $resultsearch['age']; ?></td>
            <td><?php echo $resultsearch['country']; ?></td>
            <td><?php echo $resultsearch['city']; ?></td>
            <td><?php echo $resultsearch['number']; ?></td>
            <td><a href="/user/update/<?php echo $resultsearch['user_id']; ?>" title="Редактировать">Редактировать</a>
            </td>
            <td><a href="/user/delete/<?php echo $resultsearch['user_id']; ?>" title="Удалить">Удалить</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<table class="contactTable">
    <th></th>
    <th>Contacts</th>
    <th></th>
    <th></th>
    <?php foreach ($get1 as $gets): ?>
        <tr>
            <td class="phpType"><?php echo '<b>' . $gets['type'] . ': '; ?></td>
            <td class="phpContact"><?php echo $gets['contact']; ?></td>
            <td><a href="/user/updcontacts/<?php echo $gets['user_id']; ?>/<?php echo $gets['contact_id']; ?>"
                   title="Редактировать контакт"><img class="editimage"
                                                      src="http://s1.iconbird.com/ico/0612/developer/w512h5121339360019gear.png"></a>
            </td>
            <td><a href="/user/delcontact/<?php echo $gets['user_id']; ?>/<?php echo $gets['contact_id']; ?>"
                   title="Удалить контакт"><img class="deleteimage"
                                                src="http://sazonovigor.ru/wp-content/uploads/2015/02/w256h2561339405847Delete256.png"></a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>