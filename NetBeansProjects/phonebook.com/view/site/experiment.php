<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Closure test</title>
    </head>
    <body>
        <a href="#" id="link1">Нажми</a> <a href="#" id="link2">И меня нажми</a>
        <div id="hide1">Скрой меня скорее</div>
        <div id="hide2">И меня тоже скрой</div>

        <script>
            function getHider(id) //Передаем id элемента который надо скрыть
            {
                return function ()
                {
                    document.getElementById(id).style.display = 'none';
                    return false;
                }
            }
            document.getElementById('link1').onclick = getHider('hide1');
            document.getElementById('link2').onclick = getHider('hide2');
        </script>

    </body>
</html>
