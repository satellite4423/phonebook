<!DOCTYPE html>
<html>
<head>
    <title>Обновление пользователей</title>
</head>
<body>
<p><?php include_once ROOT . '/view/site/header.php'; ?></p>
<div class="updateform">
    <h4>Редактировать пользователя #<?php echo $user_id; ?></h4>
    <form action="#" method="post" enctype="multipart/form-data">
        <p>Имя</p>
        <input type="text" name="firstname" placeholder="" value="<?php echo $users['firstname'] ?>">
        <p>Фамилия</p>
        <input type="text" name="lastname" value="<?php echo $users['lastname'] ?>">
        <p>Возраст</p>
        <input type="text" name="age" value="<?php echo $users['age'] ?>">
        <p>Страна</p>
        <input type="text" name="country" value="<?php echo $users['country'] ?>">
        <p>Город</p>
        <input type="text" name="city" value="<?php echo $users['city'] ?>">
        <p>Телефонный номер</p>
        <input type="text" name="number" value="<?php echo $users['number'] ?>">
        <br><br><input type="submit" name="submit" value="Save changes">
    </form>
</div>
</body>
</html>