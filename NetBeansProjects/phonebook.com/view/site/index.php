<?php include_once ROOT . '/view/site/header.php'; ?>
<table class="table-bordered table-striped table">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Age</th>
        <th>Country</th>
        <th>City</th>
        <th>Number</th>
        <th></th>
    </tr>
    <?php foreach ($getUsersLists as $users): ?>
        <tr>
            <td><?php echo $users['user_id']; ?></td>
            <td><?php echo $users['firstname']; ?></td>
            <td><?php echo $users['lastname']; ?></td>
            <td><?php echo $users['age']; ?></td>
            <td><?php echo $users['country']; ?></td>
            <td><?php echo $users['city']; ?></td>
            <td><?php echo $users['number']; ?></td>
            <td><a href="/personal/<?php echo $users['user_id']; ?>">Персональный просмотр</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<p class="paginationnn"><?php echo $pagination->get(); ?></p>
<form name="search" action="search" method="post">
    <input type="search" name="query" placeholder="Search">
    <input type="submit" value="Search">
</form>
<footer>
    <article class="about">Copyright</article>
    <p class="left"></p>
    <div class="centr"></div>
</footer>
</body>
</html>