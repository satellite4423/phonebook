<?php

require_once ROOT . '/components/Db.php';

class Users
{

    const SHOW_BY_DEFAULT = 5;

    
    public static function getUserById($user_id)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM infobook WHERE user_id = :user_id';
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result->fetch();
    }

    public static function getPersonal($user_id)
    {
        $db = Db::getConnection();
        $sql = ("SELECT * FROM infobook WHERE user_id = :user_id");
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result;
    }

    private static function createUser($options)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO infobook'
            . '(firstname, lastname,age,country,city,number)'
            . 'VALUES'
            . '(:firstname, :lastname, :age, :country, :city, :number)';
        $result = $db->prepare($sql);
        $result->bindParam(':firstname', $options['firstname'], PDO::PARAM_STR);
        $result->bindParam(':lastname', $options['lastname'], PDO::PARAM_STR);
        $result->bindParam(':age', $options['age'], PDO::PARAM_INT);
        $result->bindParam(':country', $options['country'], PDO::PARAM_STR);
        $result->bindParam(':city', $options['city'], PDO::PARAM_STR);
        $result->bindParam(':number', $options['number'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function deleteUserById($user_id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE * FROM infobook,contacts,contacts_types WHERE user_id = :user_id AND infobook.user_id = contacts.user_id AND contacts.contact_id = contacts_types.contact_id';
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateUserById($user_id, $options)
    {
        $db = Db::getConnection();
        $sql = "UPDATE infobook
                     SET
                     firstname = :firstname,
                     lastname = :lastname,
                     age = :age,
                     country = :country,
                     city = :city,
                     number = :number
                     WHERE user_id = :user_id";
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->bindParam(':firstname', $options['firstname'], PDO::PARAM_STR);
        $result->bindParam(':lastname', $options['lastname'], PDO::PARAM_STR);
        $result->bindParam(':age', $options['age'], PDO::PARAM_INT);
        $result->bindParam(':country', $options['country'], PDO::PARAM_STR);
        $result->bindParam(':city', $options['city'], PDO::PARAM_STR);
        $result->bindParam(':number', $options['number'], PDO::PARAM_STR);
//        $result->bindParam(':contact', $options['contact'], PDO::PARAM_STR);
//        $result->bindParam(':type', $options['type'], PDO::PARAM_STR);
        return $result->execute();
    }

    public static function searchUserBySimilarity($similarity)
    {
        $db = Db::getConnection();
        $sql = "SELECT DISTINCT(infobook.user_id) FROM infobook,contacts,contacts_types WHERE infobook.firstname = :similarity OR infobook.lastname = :similarity "
            . "OR infobook.number = :similarity OR contacts.contact = :similarity";
        $result = $db->prepare($sql);
        $result->bindParam(':similarity', $similarity, PDO::PARAM_STR);
//        $result->bindParam('infobook.user_id', $user_id,  PDO::PARAM_INT);
        $result->execute();
        $row = $result->fetch();
        return implode($sql, $row);
    }


    public static function getTotal()
    {
        $connect = Db::getConnection();
        $query = ('SELECT count(user_id) AS count FROM infobook');
        $result = $connect->prepare($query);
        $result->execute();
        $row = $result->fetch();
        return $row['count'];
    }

    public static function getUsersLists($page)
    {
        $limit = Users::SHOW_BY_DEFAULT;
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        $db = Db::getConnection();
        $sql = "SELECT user_id, firstname, lastname, age, country, city, number FROM infobook ORDER BY user_id ASC LIMIT :limit OFFSET :offset";
        $result = $db->prepare($sql);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);
        $result->execute();
        $i = 0;
        $usersfornavi = array();
        while ($row = $result->fetch()) {
            $usersfornavi[$i]['user_id'] = $row['user_id'];
            $usersfornavi[$i]['firstname'] = $row['firstname'];
            $usersfornavi[$i]['lastname'] = $row['lastname'];
            $usersfornavi[$i]['age'] = $row['age'];
            $usersfornavi[$i]['country'] = $row['country'];
            $usersfornavi[$i]['city'] = $row['city'];
            $usersfornavi[$i]['number'] = $row['number'];
            $i++;
        }
        return $usersfornavi;
    }

    public static function showcontacts($user_id)
    {
        $db = Db::getConnection();
        $sql = ("SELECT infobook.*,contacts.contact_id,contacts_types.contact_id    ,contact,type FROM infobook,contacts,contacts_types "
            . "WHERE infobook.user_id = contacts.user_id "
            . "AND contacts.contact_id = contacts_types.contact_id "
            . "AND infobook.user_id = :user_id ");
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        return $result;
    }

    public static function setContacts($options2)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO contacts (contact, user_id) VALUES (:contact, ' . Users::setUseridDb() . ')';
        $result = $db->prepare($sql);
        $result->bindParam(':contact', $options2['contact'], PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function setContactsTypes($type)
    {
        $db = Db::getConnection();
        $type = null;
        if (isset($_POST['types'])) {
            foreach ($_POST['types'] as $key => $value) {
                $type = $value;
            }
        }
        $sql = 'INSERT INTO contacts_types (contact_id, type) VALUES (' . Users::setContactidDb() . ', :type)';
        $result = $db->prepare($sql);
        $result->bindParam(':type', $type, PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function setUseridDb()
    {
        $db = Db::getConnection();
        $sql = "SELECT MAX(user_id) as maxuserid FROM infobook";
        $result = $db->prepare($sql);
        $result->execute();
        $row = $result->fetch();
        return $row['maxuserid'];
    }

    public static function setContactidDb()
    {
        $db = Db::getConnection();
        $sql = "SELECT MAX(contact_id) as maxcid FROM contacts";
        $result = $db->prepare($sql);
        $result->execute();
        $row2 = $result->fetch();
        return $row2['maxcid'];
    }

    public static function setContactToUser($user_id, $contact)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO contacts (contact, user_id) VALUES (:contact, :user_id)';
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_STR);
        $result->bindParam(':contact', $contact['contact'], PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function setTypeToUser($user_id, $type)
    {
        $db = Db::getConnection();
        $type = null;
        if (isset($_POST['types'])) {
            foreach ($_POST['types'] as $key => $value) {
                $type = $value;
            }
        }
        $sql = 'INSERT INTO contacts_types (contact_id, type) VALUES (' . Users::setContactidDb() . ', :type)';
        $result = $db->prepare($sql);
        $result->bindParam(':type', $type, PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function editSaidContactContact($contact_id, $chcontact)
    {
        $db = Db::getConnection();
        $sql = "UPDATE contacts,infobook,contacts_types SET contact = :contact WHERE contacts.contact_id = contacts_types.contact_id AND contacts.contact_id = :contact_id";
        $result = $db->prepare($sql);
        $result->bindParam(':contact_id', $contact_id, PDO::PARAM_INT);
        $result->bindParam(':contact', $chcontact['contact'], PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function editSaidContactType($contact_id, $chtype)
    {
        $db = Db::getConnection();
        $sql = "UPDATE contacts_types,contacts SET type = :type WHERE contacts_types.contact_id = contacts.contact_id AND contacts.contact_id = :contact_id";
        $result = $db->prepare($sql);
        $result->bindParam(':contact_id', $contact_id, PDO::PARAM_INT);
        $result->bindParam(':type', $chtype['type'], PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function showSaidContactByContactId($contact_id)
    {
        $db = Db::getConnection();
        $sql = "SELECT contacts_types.type,contacts.contact, contacts.contact_id FROM contacts_types,contacts WHERE contacts_types.contact_id = contacts.contact_id AND contacts.contact_id = :contact_id";
        $result = $db->prepare($sql);
        $result->bindParam(':contact_id', $contact_id, PDO::PARAM_INT);
        $result->execute();
        return $result;
    }

    public static function deleteSaidContactType($contact_id)
    {
        $db = Db::getConnection();
        $sql = "DELETE FROM contacts_types WHERE contacts_types.contact_id = :contact_id";
        $result = $db->prepare($sql);
        $result->bindParam(':contact_id', $contact_id, PDO::PARAM_INT);
        $result->execute();
        return $result;
    }

    public static function deleteSaidContactContact($contact_id)
    {
        $db = Db::getConnection();
        $sql = "DELETE FROM contacts WHERE contacts.contact_id = :contact_id";
        $result = $db->prepare($sql);
        $result->bindParam(':contact_id', $contact_id, PDO::PARAM_INT);
        $result->execute();
        return $result;
    }
}
