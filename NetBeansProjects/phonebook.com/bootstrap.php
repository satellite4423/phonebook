<?php
require_once 'vendor/autoload.php';
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
$isDevMode = true;
$conn = array(
    'driver' => 'pdo_mysql',
    'host' => 'phonebook.com',
    'dbname' => 'Phonebook',
    'user' => 'root',
    'password' => 'password',
);
$config = Setup::createXMLMetadataConfiguration(array(__DIR__. "/config/xml"), $isDevMode);
$array = (array(__DIR__."/config/xml"));
$entityManager = EntityManager::create($conn, $config);
echo 'bootstrap was included<br>';