<?php

class Pagination
{
    private $max = 10;
    private $total;
    private $limit;
    private $current_page;
    private $index = 'page';

    public function __construct($total, $limit, $currentPage, $index)
    {
        $this->total = $total;
        $this->limit = $limit;
        $this->setCurrentPage($currentPage);
        $this->index = $index;
        $this->amount = $this->amount();
    }

    private function setCurrentPage($currentPage)
    {
        $this->current_page = $currentPage;
        if ($this->current_page > 0) {
            if ($this->current_page > $this->amount()) {
                $this->current_page = $this->amount();
            } else {
                $this->current_page = 1;
            }
        }
        return $currentPage;
    }

    private function amount()
    {
        return ceil($this->total / $this->limit);
    }

    private function limits()
    {
        $left = $this->current_page - round($this->max / 2);
        $start = $left > 0 ? $left : 1;
        if ($start + $this->max <= $this->amount()) {
            $end = $start > 1 ? $start + $this->max : $this->max;
        } else {
            $end = $this->amount();
            $start = $this->amount() - $this->max > 0 ? $this->amount() - $this->max : 1;
        }
        return
            array($start, $end);
    }

    private function generateHtml($page, $text = null)
    {
        if (!$text) {
            $text = $page;
            $currentURI = rtrim($_SERVER['REQUEST_URI'], '/') . '/';
            $currentURI = preg_replace('~/page-[0-9]+~', '', $currentURI);
            return
                '<a href="' . $currentURI . $this->index . $page . '">' . $text . '</a>';
        }
    }

    public function get()
    {
        $links = null;
        $limits = $this->limits();
        for ($page = $limits[0]; $page <= $limits[1]; $page++) {
            if ($page == $this->current_page) {
                $links .= '<a href="page-1">' . $page . '</a>';
            } else {
                $links .= $this->generateHtml($page);
            }
        }
        if (!is_null($links)) {
            if ($this->current_page > 1) {
                $links = $this->generateHtml(1, '&lt;') . $links;
            }
            if ($this->current_page < $this->amount) {
                $links .= $this->generateHtml($this->amount, '&gt;');
            }
            $html = $links;
        }
        return $html;
    }
}