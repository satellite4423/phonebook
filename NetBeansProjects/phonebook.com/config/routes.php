<?php
return array(
    '' => 'user/index',
    'user/create' => 'user/create',
    'user/update/([0-9]+)' => 'user/update/$1',
    'user/delete/([0-9]+)' => 'user/delete/$1',
    'search' => 'user/search',
    'page-([0-9]+)' => 'user/navigation/$1',
    'personal/([0-9]+)' => 'user/personal/$1',
    'user/addcontacts/([0-9]+)' => 'user/addcontacts/$1',
    'user/updcontacts/([0-9]+)/([0-9]+)' => 'user/updatecontactbycontactid/$1/$2',
    'user/delcontact/([0-9]+)/([0-9]+)' => 'user/deletecontactbycontactid/$1/$2',
    'testorm' => 'user/ormusers' 
);