<?php require_once ROOT.'/controllers/UserController.php';?>

<!DOCTYPE html>
<html>
    <head>
        <title>Добавить пользователя</title>
    </head>
    <body>
        <p><?php include_once ROOT . '/view/site/header.php'; ?></p>
        <div class="create">
            <h4>Добавление пользователя</h4><hr>

            <form action="#" method="post">
                <p>Имя</p>
                <input type="text" name="firstname" placeholder="" value="">
                <p>Фамилия</p>
                <input type="text" name="lastname" placeholder="" value="">
                <p>Возраст</p>
                <input type="text" name="age" placeholder="" value="">
                <p>Страна</p>
                <input type="text" name="country" placeholder="" value="">
                <p>Город</p>
                <input type="text" name="city" placeholder="" value="">
                <p>Телефонный номер</p>
                <input type="text" name="number" placeholder="" value="">
                <br><br/><button type="submit" name="addcontact" value="Добавить допольнительные контакты">Сохранить</button>
                <br/><a href="/">На главную</a>
        </div>
    </body>
</html>