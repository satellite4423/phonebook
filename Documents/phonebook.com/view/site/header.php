<!DOCTYPE html>
<?php require_once ROOT . '/controllers/UserController.php';  ?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Основная страница</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">
    <link href="/css/price-range.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
    <link href="/css/page.css" rel="stylesheet">
    <link href="/css/footer.css" rel="stylesheet">
</head>
<body>
    <h1 class="h1" style="color: #0083C9;"><a href="/page-1">Phonebook</a></h1>
    <h5 class="h5"><a href="/user/create" class="addcontact">+Add contact</a></h5>
    <hr class="line">
