<?php

require_once ROOT . '/models/users.php';
require_once ROOT . '/components/Pagination.php';

class UserController {

    public function actionIndex() {
        header("Location: /page-1");
        return true;
    }

    public function actionCreate() {
        if (isset($_POST['submit'])) {
            $options['firstname'] = $_POST['firstname'];
            $options['lastname'] = $_POST['lastname'];
            $options['age'] = $_POST['age'];
            $options['country'] = $_POST['country'];
            $options['city'] = $_POST['city'];
            $options['number'] = $_POST['number'];
//            $options2['contact'] = $_POST['contact'];          
//            $options3['type'] = $_POST['type'];
            $errors = false;
            $id = Users::createUser($options);
//            $id2 = Users::setContacts($options2);
//            $id3 = Users::setContactsTypes($options3);
           
            header("Location: /page-5");
        }
        require_once (ROOT . '/view/site/create.php');
        return true;
    }

    public function actionUpdate($user_id) {
        $users = Users::getUserById($user_id);
        if (isset($_POST['submit'])) {
//            $options['id'] = $_POST['id'];
            $options['firstname'] = $_POST['firstname'];
            $options['lastname'] = $_POST['lastname'];
            $options['age'] = $_POST['age'];
            $options['country'] = $_POST['country'];
            $options['city'] = $_POST['city'];
            $options['number'] = $_POST['number'];
            Users::updateUserById($user_id, $options);
//            $options['other_number'] = $_POST['other_number'];
//            $options['skype'] = $_POST['skype'];
//            $options['email'] = $_POST['email'];
            header("Location: /personal/".$user_id);
        }
        require_once (ROOT . '/view/site/update.php');
        return true;
    }

    public function actionDelete($user_id) {
        if (isset($_POST['submit'])) {
            Users::deleteUserById($user_id);
            header("Location: /page-1");
        }
        require_once (ROOT . '/view/site/delete.php');
        return true;
    }

    public function actionSearch() {
        if (isset($_POST['query'])){
            header("Loccation: /personal/1");
            $similarity = $_POST['query'];
            $similarity2 = Users::searchUserBySimilarity($similarity);
            $user_id = $similarity2;
            echo '<pre>';
            print_r($user_id);
            echo '</pre>';
            $get = Users::getPersonal($user_id);
            $get1 = Users::showcontacts($user_id);
        }
        require_once ROOT.'/view/site/sresults.php';
        return true;
    }

    public function actionNavigation($page) {
        $getUsersLists = Users::getUsersLists($page);
        $total = Users::getTotal();
        $pagination = new Pagination($total, Users::SHOW_BY_DEFAULT,$page, 'page-');
        require_once ROOT . '/view/site/index.php';
        return true;
    }
    
    public function actionPersonal($user_id){
        $get1 = Users::getPersonal($user_id);
            $get = Users::showcontacts($user_id);
            $example = Users::setUseridDb();
            $example2 = Users::setContactidDb();
        require_once ROOT.'/view/site/personaluserpage.php';
        return true;
    }
    
    public function actionUpdatecontactbycontactid($user_id, $contact_id){
        $showSaid = Users::showSaidContactByContactId($contact_id);
        if (isset($_POST['editsubmit'])){
            $chtype['type'] = $_POST['type'];
            $chcontact['contact'] = $_POST['contact'];
            Users::editSaidContactType($contact_id,$chtype);
            Users::editSaidContactContact($contact_id,$chcontact);
            header('Location: /personal/'.$user_id);
        }
        require_once ROOT.'/view/site/editcontact.php';
        return true;
    }
    public function actionAddcontacts($user_id){
        if (isset($_POST['addcontact'])){
            $contact['contact'] = $_POST['contact'];
            $type['type'] = $_POST['type'];
        Users::setContactToUser($user_id, $contact);
        Users::setTypeToUser($user_id, $type);
        header("Location: /personal/".$user_id);
        }
        require_once ROOT.'/view/site/addcontacts.php';
        return true;
    }
    
    public function actionDeletecontactbycontactid($user_id, $contact_id){
        $showSaid = Users::showSaidContactByContactId($contact_id);
        if (isset($_POST['delete'])){
            try{
            Users::deleteSaidContactType($contact_id);
            Users::deleteSaidContactContact($contact_id);
            header("Location: /personal/".$user_id);}
 catch (Exception $e){
    echo 'Message: ' . $e->getMessage();
 }
        }
        require_once ROOT.'/view/site/deletecontact_question.php';
        return true;
    }
}
